INSERT INTO fill(
		fill_id, 
		strategy_id,
		fill_status,
		fill_percentage
	)VALUES(
		0,
        0,
        'FILL',
        100
);
INSERT INTO fill(
		fill_id, 
		strategy_id,
		fill_status,
		fill_percentage
	)VALUES(
		1,
        1,
        'FAIL',
        0
);
INSERT INTO fill(
		fill_id, 
		strategy_id,
		fill_status,
		fill_percentage
	)VALUES(
		2,
        2,
        'PARTIAL',
        69
);
INSERT INTO fill(
		fill_id, 
		strategy_id,
		fill_status,
		fill_percentage
	)VALUES(
		3,
        3,
        'FILL',
        100
);
SELECT * FROM fill;