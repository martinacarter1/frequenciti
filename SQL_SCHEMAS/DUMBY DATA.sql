INSERT INTO strategy(
		strategy_id, 
		trade_stock_price,
		trade_stock_size,
		trade_stock_start,
		trade_stock_name,
		trade_stock_position,
		trade_stock_status
	)VALUES(
		0,
        99.45,
        100,
        '2019-07-26 14:44:44',
        'GOOG',
        'SHORT',
        'OPEN'
);
INSERT INTO strategy(
		strategy_id, 
		trade_stock_price,
		trade_stock_size,
		trade_stock_start,
		trade_stock_name,
		trade_stock_position,
		trade_stock_status
	)VALUES(
		1,
        420.69,
        300,
        '2019-07-26 14:57:33',
        'APPL',
        'LONG',
        'OPEN'
);
INSERT INTO strategy(
		strategy_id, 
		trade_stock_price,
		trade_stock_size,
		trade_stock_start,
		trade_stock_name,
		trade_stock_position,
		trade_stock_status
	)VALUES(
		2,
        45.44,
        185,
        '2019-07-26 14:44:44',
        'BBC',
        'SHORT',
        'CLOSED'
);
INSERT INTO strategy(
		strategy_id, 
		trade_stock_price,
		trade_stock_size,
		trade_stock_start,
		trade_stock_name,
		trade_stock_position,
		trade_stock_status
	)VALUES(
		3,
        94.36,
        1000,
        '2019-07-26 14:57:11',
        'YAHO',
        'LONG',
        'CLOSED'
);
SELECT * FROM strategy;