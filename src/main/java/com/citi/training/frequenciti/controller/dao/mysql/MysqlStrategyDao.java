package com.citi.training.frequenciti.controller.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.frequenciti.controller.dao.StrategyDao;
import com.citi.training.frequenciti.controller.model.Strategy;



@Component
public class MysqlStrategyDao implements StrategyDao {
	 private static final Logger logger =
             LoggerFactory.getLogger(MysqlStrategyDao.class);

private static String FIND_ALL_SQL = "select strategy_id, trade_stock_price, " +
                          "trade_stock_size,trade_stock_start,trade_stock_name, trade_stock_position, trade_stock_status from strategy";

private static String FIND_SQL = FIND_ALL_SQL + " where stategy_id = ?";

/*private static String FIND_BY_POSITION_SQL = FIND_ALL_SQL + " where trade_stock_position = ?";*/

private static String INSERT_SQL = "INSERT INTO strategy (stategy_id, trade_stock_price, trade_stock_size,trade_stock_start, trade_stock_name, trade_stock_position, trade_stock_status, created) " +
                        "values (:stategy_id, :trade_stock_price, :trade_stock_size,:trade_stock_start, :trade_stock_name, :trade_stock_position, :trade_stock_status, :created)";

private static String UPDATE_SQL = "UPDATE strategy SET stategy_id=:stategy_id, trade_stock_price=:trade_stock_price, trade_stock_size=:trade_stock_size, " +
                        "trade_stock_start=:trade_stock_start,trade_stock_name:trade_stock_name,trade_stock_position:trade_stock_position, trade_stock_status: trade_stock_status WHERE stategy_id=:stategy_id";

private static String DELETE_SQL = "delete from strategy where stategy_id=?";

@Autowired
private JdbcTemplate tpl;

@Autowired
NamedParameterJdbcTemplate namedParameterJdbcTemplate;

public List<Strategy> findAll(){
    logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
    return tpl.query(FIND_ALL_SQL,
                     new StrategyMapper());
}


public Strategy findById(int id) {
    logger.debug("findBydId(" + id + ") SQL: [" + FIND_SQL + "]");
    List<Strategy> Trades = this.tpl.query(FIND_SQL,
            new Object[]{id},
            new StrategyMapper()
    );
    if(Trades.size() <= 0) {
        String warnMsg = "Requested Trade not found, id: " + id;
        logger.warn(warnMsg);
        return null;
        //throw new TradeNotFoundException(warnMsg);
    }
    if(Trades.size() > 1) {
        logger.warn("Found more than one Trade with id: " + id);
    }
    return Trades.get(0);
}

public int create(Strategy strategy) {
    KeyHolder keyHolder = new GeneratedKeyHolder();
    this.tpl.update(
        new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps =
                        connection.prepareStatement("insert into strategy (trade_stock_price,trade_stock_size"+
                "			trade_stock_start,trade_stock_name,trade_stock_position, trade_stock_status) values (?, ?, ?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setDouble(1, strategy.getTrade_stock_price());
                ps.setInt(2, strategy.getTrade_stock_size());
                ps.setDate(3, (java.sql.Date) strategy.getTrade_stock_start());
                ps.setString(4, strategy.getTrade_stock_name());
                ps.setString(5, strategy.getTrade_stock_position());
                ps.setString(6, strategy.getTrade_stock_status());
                return ps;
            }
        },
        keyHolder);
    return keyHolder.getKey().intValue();
}




public int save(Strategy trade) {
    MapSqlParameterSource namedParameters = new MapSqlParameterSource();

    namedParameters.addValue("stategy_id", trade.getStategy_id());
    namedParameters.addValue("trade_stock_price", trade.getTrade_stock_price());
    namedParameters.addValue("trade_stock_size", trade.getTrade_stock_size());
    namedParameters.addValue("trade_stock_start", trade.getTrade_stock_start());
    namedParameters.addValue("trade_stock_name", trade.getTrade_stock_name());
    namedParameters.addValue("trade_stock_position", trade.getTrade_stock_position());
    namedParameters.addValue("trade_stock_status", trade.getTrade_stock_status());


    if (trade.getStategy_id() < 0) {
        logger.debug("Inserting trade: " + trade);
        namedParameters.addValue("created", LocalDateTime.now());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
        trade.setStategy_id(keyHolder.getKey().intValue());
    } else {
        logger.debug("Updating trade: " + trade);
        namedParameters.addValue("stategy_id", trade.getStategy_id());
        namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);
    }

    logger.debug("Saved trade: " + trade);
    return trade.getStategy_id();
}

public void deleteById(int id) {
    logger.debug("deleteById(" + id + ") SQL: [" + DELETE_SQL +"]");
    if(this.tpl.update(DELETE_SQL, id) <= 0) {
        String warnMsg = "Failed to delete, trade not found: " + id;
        logger.warn(warnMsg);
        //throw new TradeNotFoundException(warnMsg);
    }
    else {
        for(Strategy trade: findAll()) {
            logger.debug(trade.toString());
        }
    }
}

/**
 * private internal class to map database rows to Trade objects.
 *
 */
private static final class StrategyMapper implements RowMapper<Strategy> {
    public Strategy mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Strategy(rs.getInt("stategy_id"),
        					rs.getDouble("trade_stock_price"),
        					rs.getInt("trade_stock_size"),
                            rs.getDate("trade_stock_start"), 
                            rs.getString("trade_stock_name"),
                            rs.getString("trade_stock_position"),
                            rs.getString("trade_stock_status")
                            );
    }
}
}


