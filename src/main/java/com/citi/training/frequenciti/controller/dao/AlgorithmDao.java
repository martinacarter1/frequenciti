package com.citi.training.frequenciti.controller.dao;

public interface AlgorithmDao {

	double getMean(double timeInterval);
	
}
