package com.citi.training.frequenciti.controller.dao;

import com.citi.training.frequenciti.controller.model.Strategy;
import java.util.List;

public interface StrategyDao {
    int save(Strategy strategy);

    int create(Strategy strategy);
    List<Strategy> findAll();

    Strategy findById(int id);

    void deleteById(int id);

}
