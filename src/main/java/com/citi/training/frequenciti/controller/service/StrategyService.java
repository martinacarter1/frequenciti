package com.citi.training.frequenciti.controller.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.frequenciti.controller.model.Strategy;
import com.citi.training.frequenciti.controller.dao.StrategyDao;
@Component
public class StrategyService{
	
	@Autowired
	StrategyDao strategyDao;

	public List<Strategy> findAll() {
		return strategyDao.findAll();
	}

	public int create(Strategy strategy) {return strategyDao.create(strategy);}

	public Strategy findById(int id) {return strategyDao.findById(id);}

	public void deleteById(int id) {strategyDao.deleteById(id);}
}
