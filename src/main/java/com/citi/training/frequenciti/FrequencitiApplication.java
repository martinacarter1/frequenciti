package com.citi.training.frequenciti;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.citi.training.frequenciti.pricefeed.LivePriceFeed;
import com.citi.training.frequenciti.controller.dao.StrategyDao;
import com.citi.training.frequenciti.controller.dao.mysql.MysqlStrategyDao;
import com.citi.training.frequenciti.pricefeed.*;
@SpringBootApplication
public class FrequencitiApplication {

	public static void main(String[] args) {
		Logger logger = LoggerFactory.getLogger(FrequencitiApplication.class);
		SpringApplication.run(FrequencitiApplication.class, args);
	
		StrategyDao stratdao = new MysqlStrategyDao();
		//logger.info(stratdao.findById(1));
	
	}
	
	

}
