package com.citi.training.frequenciti.pricefeed;

public interface PriceFeed {
	
	double getLatest(String tickerSymbol);

}
